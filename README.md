Логика сборки рига
------------------

реализация https://bs-face-simple.readthedocs.io/en/latest/coding.html#coding-page

реализованы схемы:
- [connect](https://bs-face-simple.readthedocs.io/en/latest/coding.html#connect)
- [driver](https://bs-face-simple.readthedocs.io/en/latest/coding.html#drivenkeyframe)
- [multiply](https://bs-face-simple.readthedocs.io/en/latest/coding.html#multiply)
- [multiply_min](https://bs-face-simple.readthedocs.io/en/latest/coding.html#multiply-min)
- [multiply_max](https://bs-face-simple.readthedocs.io/en/latest/coding.html#multiply-max)

Lists.py
--------

* расположение относительно бленд файла: `//meta/lists.py`
* используемые атрибуты:
    * [LIST_OF_SHAPES](https://bs-face-simple.readthedocs.io/en/latest/lists.html#bs_face_simple.lists.LIST_OF_SHAPES)
    * [TARGET_NAME](https://bs-face-simple.readthedocs.io/en/latest/lists.html#bs_face_simple.lists.TARGET_NAME)
    * [CTRL_NAME](https://bs-face-simple.readthedocs.io/en/latest/lists.html#bs_face_simple.lists.CTRL_NAME) - в данном случае имя арматуры лицевых контролов.
* новые атрибуты:
    * <a name="MOCAP_FACE_CAP_APPLYING"></a>**MOCAP_FACE_CAP_APPLYING** - схема применения мокап даты из текстового файла.
    * **MIRRORED_SHAPE_KEYS** - список шейпов, которые распиливаются на правый и левый с добавлением соответствующих постфиксов ``Left`` и ``Right``.

Функционал
----------

### Mirror Shape Keys

* Создаёт (в случае отсутствия) правые и левые шейпы из исходников по списку **MIRRORED_SHAPE_KEYS** с добавлением соответствующих постфиксов ``Left`` и ``Right``. 
* Распилка по градиенты, границы градиента учитываются по координате  ``location.x`` любого выделенного объекта.

### Bild rig

* Создаёт риг, необходимые условия:
    *   наличие арматуры лицевых контролов
    *   наличие [lists.py](#listspy)
* Реализуется [логика](#логика-сборки-рига)

### Apply from TXT

* Применяется мокап дата от **Яблока Face it** на выделенную арматуру лицевых контролов.
    * необходимо:
        * выбрать текстовый файл.
        * выделить саму арматуру.
    * используется [lists.MOCAP_FACE_CAP_APPLYING](#MOCAP_FACE_CAP_APPLYING)
* Режимы (панелька возникающая по клику по кнопке):
    * ``Replace animation`` - cуществующий экшн, при наличии, будет удалён. Если не использовать, то анимация будет записываться в существующий экшн.
    * ``Start frame`` - кадр с которого будут создаваться ключи.
