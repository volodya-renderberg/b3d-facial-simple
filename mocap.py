# -*- coding: utf-8 -*-

import numpy

import bpy

ACTION_NAME='Face_It_Action'

ATTR_PATH={
'tx': ('location', 0),
'ty': ('location', 1),
'tz': ('location', 2),
}

def normalization_mocap_data(data):
    """Нормализация мокап даты. """
    # for key in data:
    #     data[key]=numpy.clip(data[key], 0, 1)
    return data

def read_mocap_data_from_txt(path) -> tuple:
    """
    Returns
    -------
    tuple
        (True, {mocap_data}) или (False, comment)
    """
    mocap_data=dict()

    with open(path, 'r') as f:
        while(True):
            heads=list()
            line = f.readline()
            if not line:
                break
            if line.startswith('bs'):
                head_list = line.replace('\n', '').split(',')[1:]
                # print(line_list)
                for k in head_list:
                    mocap_data[k]=list()
            elif line.startswith('k'):
                line_list= line.strip().split(',')[12:]
                for i, item in enumerate(line_list):
                    mocap_data[head_list[i]].append(item)

    return(True, normalization_mocap_data(mocap_data))

def _calculate_position(x, x1, x2):
    return (float(x) - float(x1))/(float(x2) - float(x1))


def apply_mocap_data(armature, mocap_data, mocap_setting, start_frame=0, replace=True) -> tuple:
    """
    Запекание мокап даты на контролы управления.

    Parameters
    ----------
    armature : bpy.types.Object
        Арматура лицевого рига.
    mocap_data : dict
        мокап дата считанная из текстового файла.
    mocap_setting : dict
        lists.MOCAP_FACE_CAP_APPLYING
    start_frame : int, optional
        это старт фрейм
    replace : bool, optional
        Если True, то старая анимация будет удалена.
    """
    if replace and armature.animation_data:
        if armature.animation_data.action:
            action=armature.animation_data.action
            bpy.data.actions.remove(action)
        armature.animation_data_clear()
    
    if not armature.animation_data:
        armature.animation_data_create()

    if not armature.animation_data.action:
        action=bpy.data.actions.new(f"{armature.name}_{ACTION_NAME}")
        if action:
            armature.animation_data.action=action
    else:
        action=armature.animation_data.action

    frames = len(mocap_data[tuple(mocap_data.keys())[0]])
    for i in range(frames):
        #(get frame data)
        frame_data=dict()
        for key in mocap_setting:
            if not key in mocap_data:
                print(f"key {key} - not found!")
                continue
            value=_calculate_position(mocap_data[key][i], mocap_setting[key][2][0], mocap_setting[key][2][1])
            new_key = f"{mocap_setting[key][0]}:{mocap_setting[key][1]}"
            if new_key in frame_data:
                if abs(value)>abs(frame_data[new_key]):
                    frame_data[new_key]=value
            else:
                frame_data[new_key]=value
        print(i)
        #(make keyframes)
        for key in frame_data:
            bone, attr=key.split(':')
            path=f'pose.bones["{bone}"].{ATTR_PATH[attr][0]}'
            index=ATTR_PATH[attr][1]
            fc = action.fcurves.find(path, index=index)
            if not fc:
                fc=action.fcurves.new(path, index=index)
            fc.keyframe_points.insert(start_frame+i, frame_data[key])

    return(True, "Ok!")
