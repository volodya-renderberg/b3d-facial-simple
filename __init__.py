#
#
#
#

# import os

# os.environ["FTRACK_DOCS_PATH"]=os.path.join(os.path.dirname(__file__), "html", "manual.html")

bl_info = {
    "name": "B3d facial simple",
    "description": "Simple facial rig",
    "author": "Volodya Renderberg",
    "version": (1, 0),
    "blender": (2, 90, 0),
    "location": "View3d tools panel",
    "warning": "", # used for warning icon and text in addons panel
    # "doc_url":"https://ftrack-b3d-connect.readthedocs.io/ru/latest/index.html",
    "category": "Rigging"}

if "bpy" in locals():
    import importlib
    importlib.reload(ui)
else:
    from . import ui

import bpy


##### REGISTER #####

def register():
    ui.register()

def unregister():
    ui.unregister()

if __name__ == "__main__":
    register()

