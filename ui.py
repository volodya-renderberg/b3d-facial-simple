# -*- coding: utf-8 -*-

import os
import sys
import webbrowser
from importlib import reload

import bpy

from . import working
from . import mocap

class G():
    lists=None

def get_lists():
    meta_path=(os.path.join(os.path.dirname(bpy.data.filepath), 'meta'))
    if not meta_path in sys.path:
        sys.path.append(meta_path)
    import lists
    reload(lists)
    import lists
    G.lists=lists

def set_mocap_txt(self, context):
    get_lists()
    mocap_data=mocap.read_mocap_data_from_txt(self.mocap_file)[1]
    # print(len(mocap_data[tuple(mocap_data.keys())[0]]))
    # print(mocap_data[tuple(mocap_data.keys())[0]])
    print(mocap.apply_mocap_data(
        bpy.data.objects[G.lists.CTRL_NAME],
        mocap_data,
        G.lists.MOCAP_FACE_CAP_APPLYING))

def set_params():
    bpy.types.Scene.mocap_file = bpy.props.StringProperty(name = 'Mocap TXT', subtype='FILE_PATH', default=os.path.expanduser('~'), update = None)

class FacialSimple_main_panel(bpy.types.Panel):
    bl_label = 'Fasial Simple:'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'FS'
    #bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)

        row = col.row(align = True)
        row.label(text='')
        row.operator('facialsimple.manual', icon='QUESTION')

        col.label(text='oplya')
        
        col.operator("facialsimple.mirror_shape_keys")
        col.operator("facialsimple.make_rig")
        col.prop(context.scene, "mocap_file")
        col.operator("facialsimple.apply_face_cap_mocap")

class FacialSimple_make_rig(bpy.types.Operator):
    bl_idname = "facialsimple.make_rig"
    bl_label = "Make Rig"

    def execute(self, context):
        get_lists()
        if hasattr(G.lists, "TARGET_NAME") and G.lists.TARGET_NAME:
            working.drivers_generate(bpy.data.objects[G.lists.TARGET_NAME], G.lists, bpy.data.objects[G.lists.CTRL_NAME])
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

class FacialSimple_apply_face_cap_mocap(bpy.types.Operator):
    bl_idname = "facialsimple.apply_face_cap_mocap"
    bl_label = "Apply from TXT"

    start_frame: bpy.props.IntProperty(name="Start Frame", default=0)
    replace_animation: bpy.props.BoolProperty(name="Replace Animation")
    # filepath: bpy.props.StringProperty(name = 'Path to TXT', subtype='FILE_PATH', default=os.path.expanduser('~'), update=execute)

    def execute(self, context):
        # (1)
        get_lists()
        mocap_data=mocap.read_mocap_data_from_txt(context.scene.mocap_file)[1]
        b,r=mocap.apply_mocap_data(
            # bpy.data.objects[G.lists.CTRL_NAME],
            bpy.context.object,
            mocap_data,
            G.lists.MOCAP_FACE_CAP_APPLYING,
            start_frame=self.start_frame,
            replace=self.replace_animation)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        col.prop(self, "replace_animation")
        col.prop(self, "start_frame")
        # col.prop(self, "filepath")

class FacialSimple_mirror_shape_keys(bpy.types.Operator):
    bl_idname = "facialsimple.mirror_shape_keys"
    bl_label = "Mirror Shape Keys"

    def execute(self, context):
        # (1)
        get_lists()
        b,r=working.mirror_shape_keys(context, G.lists)
        if b:
            self.report({'INFO'}, r)
        else:
            self.report({'WARNING'}, r)
        return{'FINISHED'}

#https://gitlab.com/volodya-renderberg/b3d-facial-simple/-/blob/master/README.md
class FacialSimple_manual(bpy.types.Operator):
    bl_idname = "facialsimple.manual"
    bl_label = "Manual"
    
    def execute(self, context):
        webbrowser.open_new_tab(f'https://gitlab.com/volodya-renderberg/b3d-facial-simple/-/blob/master/README.md')
        return{'FINISHED'}

### TEMPLATE
class FacialSimple_template_operator(bpy.types.Operator):
    bl_idname = "facialsimple.template_operator"
    bl_label = "Text"

    def execute(self, context):
        # (1)
        get_lists()
        self.report({'INFO'}, "template_operator")
        return{'FINISHED'}

def register():
    bpy.utils.register_class(FacialSimple_main_panel)
    bpy.utils.register_class(FacialSimple_make_rig)
    bpy.utils.register_class(FacialSimple_apply_face_cap_mocap)
    bpy.utils.register_class(FacialSimple_mirror_shape_keys)
    bpy.utils.register_class(FacialSimple_manual)
    set_params()

def unregister():
    bpy.utils.unregister_class(FacialSimple_main_panel)
    bpy.utils.unregister_class(FacialSimple_make_rig)
    bpy.utils.unregister_class(FacialSimple_apply_face_cap_mocap)
    bpy.utils.unregister_class(FacialSimple_mirror_shape_keys)
    bpy.utils.unregister_class(FacialSimple_manual)