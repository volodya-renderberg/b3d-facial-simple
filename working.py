# -*- coding: utf-8 -*-

import os
import sys

import bpy

ATTRS={
'tx': 'location[0]',
'ty': 'location[1]',
'tz': 'location[2]',
}

def _remove_driver_from_shape_key(ob, key_name):
    """Удаление драйвера объекта **ob** с шейпкея по имени **name**. """
    try:
        ob.data.shape_keys.driver_remove(f'key_blocks["{key_name}"].value')
    except Exception as e:
        print(e)

def _remove_driver_from_custom_prop(ob, key_name):
    """Удаление драйвера объекта **ob** с шейпкея по имени **name**. """
    try:
        ob.driver_remove(f'["{key_name}"]')
    except Exception as e:
        print(e)

def _add_driver_to_custom_prop(ob, prop_name, expression):
    """Создание драйвера на кастомный атрибут объекта.

    Returns
    -------
    tuple
        (f_curve, драйвер)
    """
    #(remove)
    _remove_driver_from_custom_prop(ob, prop_name)
    #(create)
    f_curve = ob.driver_add(f'["{prop_name}"]')
    #drv
    drv = f_curve.driver
    drv.type = 'SCRIPTED'
    drv.expression = expression
    #
    try:
        fmod = f_curve.modifiers[0]
        f_curve.modifiers.remove(fmod)
    except:
        pass
    #
    return f_curve, drv

def _add_driver_to_shape_key(ob, key_name, expression):
    """Создание драйвера на шейпкей

    Returns
    -------
    tuple
        (f_curve, драйвер)
    """
    #(remove)
    _remove_driver_from_shape_key(ob, key_name)
    #(create)
    f_curve = ob.data.shape_keys.key_blocks[key_name].driver_add('value')
    #drv
    drv = f_curve.driver
    drv.type = 'SCRIPTED'
    drv.expression = expression
    #
    try:
        fmod = f_curve.modifiers[0]
        f_curve.modifiers.remove(fmod)
    except:
        pass
    #
    return f_curve, drv

def _add_point_on_fcurve(f_curve, x, y, interpolation='LINEAR'):
    """Добавление точки на f_curve.

    Parameters
    ----------
    f_curve : f_curve
        f_curve драйвера
    x : float, int
        x
    y : float, int
        y
    interpolation : str, optional
        Тип интерполяции.
    
    Returns
    -------
    None
    """
    point = f_curve.keyframe_points.insert(x,y)
    point.interpolation = interpolation

def _add_variable_to_driver(drv, var_name, target_name, target_attr, armature=False, shape_key=False, var_type="SINGLE_PROP"):
    """
    Parameters
    ----------
    drv : driver
        драйвер
    var_name : str
        имя переменной
    target_name : str
        Имя объекта влияния, в случае если передаётся арматура - это имя кости.
    target_attr : str
        атрибут влияния, в случае трансформов записываются в стиле *maya* короткие имена (tx, ty ..). В случае shape_key - это имя шейпкея.
    armature : Armature, optional
        Арматура - если передавать, то объект влияния будет костью этой арматуры, если не передавать - то сам объект.
    shape_key : bool
        Если True - то data_path формируется соответствующим образом.
    var_type : str, optional
        тип переменной, какие бывают.

    """
    #var
    var = drv.variables.new()
    var.name = var_name
    var.type = var_type
    #targ
    targ = var.targets[0]
    if armature:
        targ.id = armature
        # targ.bone_target=item[1][1]
    else:
        targ.id = bpy.data.objects[target_name]
    if target_attr in ATTRS:
        attr=ATTRS[target_attr]
    else:
        attr=target_attr
    if armature:
        targ.data_path=f'pose.bones["{target_name}"].{attr}'
    elif shape_key:
        targ.data_path=f'data.shape_keys.key_blocks["{attr}"].value'
    else:
        targ.data_path=attr

def drivers_generate(ob, lists, armature=False):
    """ Создание драйверов на shape_keys.
    
    Parameters
    ----------
    ob : bpy.data.object
        Объект на бленды которого создаются драйвера.
    lists : lists.py
        Модуль настроек.
    armature : Armature, option
        Объект арматуры от костей которого создаются драйвера.
    
    Returns
    -------
    tuple
        (True, Ok) или (False, coment)
    """
    if hasattr(lists, 'CTRL_FACTOR'):
        ctrl_factor=lists.CTRL_FACTOR
    else:
        ctrl_factor=1

    for item in lists.LIST_OF_SHAPES.items():
        if not item[0] in ob.data.shape_keys.key_blocks.keys():
            print(f'key: "{item[0]}" no found ')
            continue
        #(make driver)
        if item[1][0]=='connect':
            f_curve, drv=_add_driver_to_shape_key(ob, item[0], f'var*{ctrl_factor}')
            _add_variable_to_driver(drv, 'var', item[1][1], item[1][2], armature=armature)
        elif item[1][0]=='driver':
            f_curve, drv=_add_driver_to_shape_key(ob, item[0], f'var*{ctrl_factor}')
            _add_variable_to_driver(drv, 'var', item[1][1][0], item[1][1][1], armature=armature)
            for point in item[1][1][2].items():
                # print(point)
                _add_point_on_fcurve(f_curve, float(point[0]), point[1])
        elif item[1][0].startswith('multiply'):
            props=list()
            vars_=list()
            for i, data in enumerate(item[1][1]):
                #(make prop)
                prop_name=f'{data[0]}_{data[1]}_{item[0]}'
                props.append(prop_name)
                vars_.append(f'var{i}')
                ob[prop_name]=0.0
                #(make prop driver)
                f_curve, drv=_add_driver_to_custom_prop(ob, prop_name, f'var*{ctrl_factor}')
                if item[0].startswith('correct_'):
                    _add_variable_to_driver(drv, 'var', ob.name, data[0], shape_key=True)
                else:
                    _add_variable_to_driver(drv, 'var', data[0], data[1], armature=armature)
                for point in data[2].items():
                    # print(point)
                    _add_point_on_fcurve(f_curve, float(point[0]), point[1])

            #(make multiply driver)
            if item[1][0].endswith('_min'):
                f_curve, drv=_add_driver_to_shape_key(ob, item[0], f"1*min({','.join(vars_)})")
            elif item[1][0].endswith('_max'):
                f_curve, drv=_add_driver_to_shape_key(ob, item[0], f"1*max({','.join(vars_)})")
            else:
                f_curve, drv=_add_driver_to_shape_key(ob, item[0], '*'.join(vars_))
            for i, var in enumerate(props):
                _add_variable_to_driver(drv, f'var{i}', ob.name, f'["{var}"]')
    # (end)
    print('drivers created')
    return(True, 'Ok!')

# if hasattr(lists, "TARGET_NAME") and lists.TARGET_NAME:
#     drivers_generate(bpy.data.objects[lists.TARGET_NAME], bpy.data.objects[lists.CTRL_NAME])

def mirror_shape_keys(context, lists):
    ob=bpy.context.object
    if not lists.TARGET_NAME in bpy.data.objects:
        return(False, f'No found object "{lists.TARGET_NAME}"')
    mesh=bpy.data.objects[lists.TARGET_NAME]
    for key in lists.MIRRORED_SHAPE_KEYS:
        if not key in mesh.data.shape_keys.key_blocks:
            print(f'shape_key "{key}" not found!')
            continue
        shkey_m=mesh.data.shape_keys.key_blocks[key]
        #(vars)
        x2=abs(ob.location[0])
        x1=-x2
        #(calculate)
        for side in ('Left','Right'):
            name=f'{key}{side}'
            if name in mesh.data.shape_keys.key_blocks:
                shkey_side=mesh.data.shape_keys.key_blocks[name]
            else:
                shkey_side=mesh.shape_key_add(name=name)
            for i,v in enumerate(mesh.data.vertices):
                x=v.co[0]
                y=v.co[1]
                z=v.co[2]
                if 'l' in side.lower():
                    if x<x1:
                        f=0
                    elif x1<x<x2:
                        f=(x-x1)/(x2-x1)
                    else:
                        f=1
                else:
                    if x<x1:
                        f=1
                    elif x1<x<x2:
                        f=1-(x-x1)/(x2-x1)
                    else:
                        f=0
                p=shkey_m.data[i].co[:]
                shkey_side.data[i].co[0]=x+(p[0]-x)*f
                shkey_side.data[i].co[1]=y+(p[1]-y)*f
                shkey_side.data[i].co[2]=z+(p[2]-z)*f


    return(True, 'Ok!')